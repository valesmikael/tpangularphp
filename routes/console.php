<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

// Artisan::command('inspire', function () {
//     $this->comment(Inspiring::quote());
// })->describe('Display an inspiring quote');


Artisan::command('dummies', function () {

    $categories = factory(App\Category::class, 8)->create();
    $products = factory(App\Product::class, 20)->create();

    foreach( $products as $product ){
        $categories_rand = $categories->random(3);
        $product->categories()->attach( $categories_rand[0] );
        $product->categories()->attach( $categories_rand[1] );
        $product->categories()->attach( $categories_rand[2] );
    }


})->describe('Create fake datas');