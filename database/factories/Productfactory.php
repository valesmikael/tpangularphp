<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [

        'titre'=> $faker->word,
        'marque'=>$faker->word,
        'descriptif'=>$faker->text(100),
        'prix'=>$faker->randomNumber(3),
        'reference'=>$faker->iban(),
        
    ];
});
