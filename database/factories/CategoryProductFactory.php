<?php

use Faker\Generator as Faker;

$factory->define(App\CategoryProduct::class, function (Faker $faker) {

    $product = factory(App\Product::class)->create();
    $category = App\Category::all()->random();

    return [
        
        'category_id'=> $category->id,
        'product_id'=> $product->id,
        
    ];
});
